# This is a script to play with JSON data and extract Chuck Norris jokes

import requests
import time

api_url = 'https://api.api-ninjas.com/v1/chucknorris'

while True:
    # Attempt to pull a Chuck Norris joke
    response = requests.get(api_url, headers={'X-Api-Key': 'p9GwgU9tafgwGAefPJVtWg==bNkPuBcrY9QFUCFV'}).json()
    # Provide time to read the joke
    time.sleep(4)
    # Some requests may not be valid
    if response['joke']:
        print(response['joke'])
